<?php 
/**
 * 
 */
 class Main extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 	}
 	
 	public function index()
 	{
 		$data['title']='Dashboard';
 		$data['body']='backend/index/index';
 		$this->load->vars($data);
 		$this->load->view('layout/back');
 	}
 	public function profile()
 	{
 		$data['title']='Profile';
 		$data['body']='backend/index/profile';
 		$this->load->vars($data);
 		$this->load->view('layout/back');
 	}
 } ?>