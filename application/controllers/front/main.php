<?php 
/**
 * 
 */
 class Main extends CI_Controller
 {
 	
 	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['featured_products']=Product::find('all',array('conditions'=>'isfeatured=1','limit'=>4));
		$data['title']='Home';

		$data['body']='frontend/index';
		
		$this->load->vars($data);
		
		 $this->load->view('layout/front');
	}
	public function about()
	{
		$data['title']='About';

		$data['body']='frontend/about';
		
		$this->load->vars($data);
		
		$this->load->view('layout/front');
	}
	public function cart()
	{
		$data['title']='Cart';

		$data['body']='frontend/cart';
		
		$this->load->vars($data);
		
		$this->load->view('layout/front');
	}
	public function contact()
	{
		$data['title']='Contact';

		$data['body']='frontend/contact';
		
		$this->load->vars($data);
		
		$this->load->view('layout/front');
	}
 } ?>