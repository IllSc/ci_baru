<?php /**
* 
*/
class Products extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function detail($id)
	{
		$data['product']=Product::find($id);
		$data['body']='frontend/product_detail';
		$data['title']='Edit Product '.$data['product']->name;
		$this->load->vars($data);
		
		$this->load->view('layout/front');
	}
} ?>