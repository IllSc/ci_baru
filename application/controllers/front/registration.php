<?php /**
* 
*/
class Registration extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function sign_up()
	{
		
		$new_user = new User();
		$input= $this->input->post('user');
		
		$new_user->validatePassword($input['password'],$this->input->post('confirm_password') );

		$new_user->email =$input['email'];
		$new_user->name=$input['name'];
		$new_user->username=$input['username'];
		$new_user->activated=0;
		
		if($new_user->save()){
			$this->session->set_flashdata('msg_type','success');
			$this->session->set_flashdata('msg_content', 'Success!!');
			
			$this->load->library('email');
			$this->email->from('admin@suitshop.com', 'Admin Suitshop');
			$this->email->to($new_user->email);
			//$this->email->cc('xcho85@gmail.com');
			//$this->email->bcc('them@their-example.com');

			$this->email->subject('Aktivasi User');
			$this->email->message('Testing the email class CodeIgniter dengan mail');

			$this->email->send();


		} else {
			
			$this->session->set_flashdata('msg_type','error');

		}
		// redirect(site_url('front/main'));
	}
} ?>