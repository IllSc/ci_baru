<?php 
	/**
	* 
	*/
	class Category extends ActiveRecord\Model
	{
		
		static $has_many = array(
			array('children', 'class_name' => 'category', 'foreign_key' => 'parent_id'),
			array('products')
			);

		static $belongs_to = array(
			array('children', 'class_name' => 'category', 'foreign_key' => 'parent_id')
			);
	}
 ?>