<div class="about">
                        <div class="container">
                            <div class="row">
                                <div class="span12">

                                    <h2 class ="textHead">Contact</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span6">
                                    <div id="map"></div>
                                </div>
                                <div class="span6">
                                    <div class="message">
                                        <b>Greeting!</b>
                                    <br><br>
                                    <p><b>SuitShop</b></p>
                                    <p>Graha Prawira 2nd Floor </p>
                                    <p>Jalan Mampang </p>
                                    <p>Jakarta, Indonesia </p>
                                    <p><a href="mailto:contact@suitshop.com">contact@suitshop.com</a></p>
                                    <br><br>
                                    <div class="author">
                                        Ivan Putera Masli <br>
                                        Fasilkom UI
                                    </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>