 <div class="product-show">
            <div class="container">
                <div class="span12">
                    <h3 class="product-title">
                        Produk Pilihan
                    </h3>
                    <div class="row">
                        <?php foreach ($featured_products as $product) :?> 
                          <div class="span3 hoverGambar">
                            <a href="<?php echo site_url('front/products/detail/'.$product->id); ?>"><span class="harga"><?php echo $product->price?></span>

                            <img src="<?php echo $product->image_url() ?>">
                            <?php echo $product->description ?></a></div>


                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="span12">
                    <h3 class="product-title">
                        Produk Terbaru
                    </h3>
                    <div class="row">
                        <div class="span3 hoverGambar">
                            <a href="#"><span class="harga">Rp 150.000,00</span> <img src="<?php echo base_url('assets/img/').'/product-1-small.jpg' ?>" Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, laudantium assumenda facilis eius. Iste, numquam, ab aliquam mollitia nostrum quas autem aspernatur eaque non quia consequuntur recusandae voluptatem beatae at.</a>
                        </div>
                        <div class="span3 hoverGambar">
                            <a href="#"><span class="harga">Rp 150.000,00</span> <img src="<?php echo base_url('assets/img/').'/product-1-small.jpg' ?>"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, pariatur accusamus rerum eaque perspiciatis nam qui. Quasi, eaque, sunt corporis inventore animi error assumenda beatae expedita enim commodi obcaecati esse!</a>
                        </div>
                        <div class="span3 hoverGambar">
                            <a href="#"><span class="harga">Rp 150.000,00</span> <img src="<?php echo base_url('assets/img/').'/product-1-small.jpg' ?>"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, molestiae, enim nobis odio culpa deserunt neque dolor est omnis officia cum voluptatum placeat. Earum, debitis laboriosam quae quos dolore assumenda.</a>
                        </div>
                        <div class="span3 hoverGambar">
                            <a href="#"><span class="harga">Rp 150.000,00</span> <img src="<?php echo base_url('assets/img/').'/product-1-small.jpg' ?>"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, enim, doloremque possimus animi aut suscipit incidunt temporibus repudiandae officiis tempore? In, numquam voluptatum ducimus tenetur itaque doloribus impedit quasi sint!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>